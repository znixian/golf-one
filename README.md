# Golf одни
ASCII only: Golf One

## A Multi-charset golfing language.
Golf один is a golfing language. It's unusual
property is that it uses multiple character
sets as part of the language, as follows:

- Greek: Flow Control
- Latin: Function names
- Cyrillic: Variable names

## Golfspec
Otherwise known as the language specification:

(Functon Νame) (Variable) (Variable) ς

### List of essential Functions

- ρ Sets a variable (1st arg) to the second argument.
- ε Adds one or more variables, and store the
result in another (arg1).
- Δ Takes a variable (arg1), and add the supplied values.
- π Prints out the value for each variable, tab seperated.

### Literal value format

- Numbers: ~123456
- Strings: "abc αβδ"

TODO: build the rest of the specification
