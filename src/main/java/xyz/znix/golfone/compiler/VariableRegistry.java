package xyz.znix.golfone.compiler;

import java.util.HashMap;
import java.util.Map;

/**
 * A registry to hold the memory positions for all usable variables.
 * <p>
 * Created by znix on 3/13/17.
 */
public class VariableRegistry {
	private final Map<String, Integer> addresses;
	private final Map<Integer, byte[]> values;
	private int offset;

	public VariableRegistry() {
		addresses = new HashMap<>();
		values = new HashMap<>();
	}

	/**
	 * Get the address of a constant.
	 *
	 * @param name The name of the variable
	 * @param data The value of the constant
	 * @return The offset in memory that the variable should be stored in.
	 */
	public int getConstantAddress(String name, byte[] data) {
		int addr = getVariableAddress(name, data.length);
		values.put(addr, data);
		return addr;
	}

	/**
	 * Get the address of a certain variable, given it's name, assuming a 32-bit value.
	 *
	 * @param name The name of the variable
	 * @return The offset in memory that the variable should be stored in.
	 */
	public int getVariableAddress(String name) {
		return getVariableAddress(name, 4);
	}

	/**
	 * Get the address of a certain variable, given it's name and width.
	 * TODO: throw an exception when trying to access a pre-exisiting variable using a different width.
	 *
	 * @param name  The name of the variable
	 * @param width The width of the variable
	 * @return The offset in memory that the variable should be stored in.
	 */
	public int getVariableAddress(String name, int width) {
		return addresses.computeIfAbsent(name, n -> offset += width);
	}
}
