package xyz.znix.golfone.compiler.inst;

import xyz.znix.golfone.compiler.Arg;
import xyz.znix.golfone.compiler.CompilationEnvironment;
import xyz.znix.golfone.compiler.CompilationException;

import java.util.List;

import static xyz.znix.golfone.compiler.inst.Utils.*;

/**
 * ρ - Set a value operator.
 * <p>
 * Created by znix on 3/13/17.
 */
public class InstSetValue implements IInst {
	@Override
	public void process(CompilationEnvironment environment, List<Arg> arguments)
			throws CompilationException {
		checkArgsLength(arguments, 2);

		System.out.println("Set variable " + arguments.get(0) + " to " + arguments.get(1));
	}
}
