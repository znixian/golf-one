package xyz.znix.golfone.compiler.inst;

import xyz.znix.golfone.compiler.Arg;
import xyz.znix.golfone.compiler.CompilationException;

import java.util.List;

/**
 * Created by znix on 3/13/17.
 */
public class Utils {
	private Utils() {
	}

	public static void checkArgsLength(List<Arg> args, int num) throws CompilationException {
		// TODO make error more descriptive
		if (args.size() != num) throw new CompilationException("Bad number of arguments - expected" + num);
	}
}
