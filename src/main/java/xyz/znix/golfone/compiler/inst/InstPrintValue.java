package xyz.znix.golfone.compiler.inst;

import xyz.znix.golfone.compiler.Arg;
import xyz.znix.golfone.compiler.CompilationEnvironment;
import xyz.znix.golfone.compiler.CompilationException;

import java.util.List;

import static xyz.znix.golfone.compiler.inst.Utils.checkArgsLength;

/**
 * ρ - Set a value operator.
 * <p>
 * Created by znix on 3/13/17.
 */
public class InstPrintValue implements IInst {
	@Override
	public void process(CompilationEnvironment environment, List<Arg> arguments)
			throws CompilationException {
		StringBuilder output = new StringBuilder("Values: ");

		arguments.stream()
				.map(a -> a.getType() + ": " + a.getOffset())
				.forEach(output::append);

		System.out.println(output.toString());
	}
}
