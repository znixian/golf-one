package xyz.znix.golfone.compiler.inst;

import xyz.znix.golfone.compiler.Arg;
import xyz.znix.golfone.compiler.CompilationEnvironment;
import xyz.znix.golfone.compiler.CompilationException;

import java.util.List;

/**
 * A single instruction that can exist in a valid program.
 * <p>
 * Created by znix on 3/13/17.
 */
public interface IInst {
	void process(CompilationEnvironment environment, List<Arg> arguments)
			throws CompilationException;
}
