package xyz.znix.golfone.compiler;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;

/**
 * A environment for everything that's happening, such as a variable registry.
 * <p>
 * Created by znix on 3/13/17.
 */
public class CompilationEnvironment {
	private final VariableRegistry variables;
	private int index;

	public CompilationEnvironment(VariableRegistry variables) {
		this.variables = variables;
	}

	@SuppressWarnings("unused")
	public VariableRegistry getVariables() {
		return variables;
	}

	public Reader interceptReader(Reader parent) {
		return new Reader() {
			int marked;

			private int progress(int num) {
				index += num;
				return num;
			}

			@Override
			public void close() throws IOException {
				parent.close();
			}

			@Override
			public int read() throws IOException {
				progress(1);
				return parent.read();
			}

			@Override
			public boolean ready() throws IOException {
				return parent.ready();
			}

			@Override
			public boolean markSupported() {
				return parent.markSupported();
			}

			@Override
			public void mark(int readAheadLimit) throws IOException {
				marked = index;
				parent.mark(readAheadLimit);
			}

			@Override
			public void reset() throws IOException {
				index = marked;
				parent.reset();
			}

			@Override
			public int read(char[] cbuf, int off, int len) throws IOException {
				return progress(parent.read(cbuf, off, len));
			}

			@Override
			public int read(CharBuffer target) throws IOException {
				return progress(parent.read(target));
			}

			@Override
			public int read(char[] cbuf) throws IOException {
				return progress(parent.read(cbuf));
			}

			@Override
			public long skip(long n) throws IOException {
				return progress((int) parent.skip(n));
			}
		};
	}

	/**
	 * Returns the index of the last read character.
	 *
	 * @return the index of the last read character.
	 */
	public int getIndex() {
		return index - 1;
	}
}
