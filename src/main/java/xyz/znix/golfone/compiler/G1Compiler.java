package xyz.znix.golfone.compiler;

import xyz.znix.golfone.compiler.inst.IInst;
import xyz.znix.golfone.compiler.inst.InstPrintValue;
import xyz.znix.golfone.compiler.inst.InstSetValue;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/**
 * Compile a program into bytecode.
 * <p>
 * Created by znix on 3/13/17.
 */
public class G1Compiler {
	private final Map<Integer, IInst> instructions;

	public G1Compiler() {
		instructions = new HashMap<>();

		registerInstruction('ρ', new InstSetValue());
		registerInstruction('π', new InstPrintValue());
	}

	public void registerInstruction(int value, IInst inst) {
		instructions.put(value, inst);
	}

	public void registerInstruction(char value, IInst inst) {
		registerInstruction((int) value, inst);
	}

	public void compile(String program) throws CompilationException {
		Reader reader = new StringReader(program);
		try {
			compile(reader);
		} catch (IOException e) {
			throw new RuntimeException("IOExec from StringReader!", e);
		}
	}

	private void compile(Reader program) throws IOException, CompilationException {
		CompilationEnvironment env = new CompilationEnvironment(
				new VariableRegistry()
		);

		program = env.interceptReader(program);

		while (program.ready() && readInstruction(env, program));
	}

	private boolean readInstruction(CompilationEnvironment env, Reader program)
			throws IOException, CompilationException {
		int icode = program.read();
		if (icode == -1) return false;
		IInst inst = instructions.get(icode);
		List<Arg> args = readArgs(env, program);
		inst.process(env, args);

		return true;
	}

	private List<Arg> readArgs(CompilationEnvironment env, Reader program) throws
			IOException, CompilationException {
		List<Arg> args = new ArrayList<>();

		while (readArg(env, program, args)) ;

		return args;
	}

	private boolean readArg(CompilationEnvironment env, Reader program, List<Arg> args)
			throws IOException, CompilationException {
		char type = (char) program.read();

		// Standard cyrillic = variable name
		if (isCyrillic(type)) {
			// Read the variable name
			String name = readWhile(program, G1Compiler::isCyrillic);

			// TODO currently assuming 4-byte variables
			int address = env.getVariables().getVariableAddress(name);
			args.add(new Arg(Arg.Type.INTEGER, address));
			return true;
		}

		switch (type) {
			case 'ς':
				return false; // End of arguments list
			case '~': {
				// TODO comment!
				String number = readWhile(program, G1Compiler::isNumber);
				int value = Integer.parseInt(number);
				byte[] bytes = ByteBuffer.allocate(4).putInt(value).array();
				int address = env.getVariables().getConstantAddress(number, bytes);
				args.add(new Arg(Arg.Type.INTEGER, address));
				break;
			}
			case '"':
				throw new UnsupportedOperationException("String not supported yet!");
			default:
				throw new CompilationException("Unsupported argtype: " + type +
						" (" + ((int) type) + ") at index " + env.getIndex());
		}

		return true;
	}

	/**
	 * Tests if the specified character is cyrillic.
	 *
	 * @param c The character to test
	 * @return The result of the check.
	 */
	private static boolean isCyrillic(char c) {
		return 0x400 <= c && c <= 0x4FF;
	}

	/**
	 * Tests if the specified character is a arabic numeral.
	 *
	 * @param c The character to test
	 * @return The result of the check.
	 */
	private static boolean isNumber(char c) {
		return '0' <= c && c <= '9';
	}

	/**
	 * Read a string from a reader, as long at a condition remains present.
	 *
	 * @param reader The reader to read from
	 * @param test   The condition to keep reading upon.
	 * @return The string read
	 * @throws IOException Errors reading the reader.
	 */
	private String readWhile(Reader reader, Predicate<Character> test) throws IOException {
		reader.mark(255);

		StringBuilder str = new StringBuilder();

		while (true) {
			char val = (char) reader.read();
			if (!test.test(val)) break;
			str.append(val);
		}

		reader.reset();
		reader.skip(str.length());

		return str.toString();
	}

}
