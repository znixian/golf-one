package xyz.znix.golfone.compiler;

/**
 * Represents the argument for a function.
 * <p>
 * Created by znix on 3/13/17.
 */
public class Arg {
	private final Type type;
	private final int offset;

	public Arg(Type type, int offset) {
		this.type = type;
		this.offset = offset;
	}

	public Type getType() {
		return type;
	}

	public int getOffset() {
		return offset;
	}

	public enum Type {
		INTEGER, STRING
	}
}
