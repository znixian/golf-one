package xyz.znix.golfone.compiler;

/**
 * An exception arising from the compilation of something.
 * <p>
 * Created by znix on 3/13/17.
 */
public class CompilationException extends Exception {
	public CompilationException(String message) {
		super(message);
	}

	public CompilationException(String message, Throwable cause) {
		super(message, cause);
	}
}
