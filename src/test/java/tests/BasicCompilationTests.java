package tests;

import org.junit.Test;
import xyz.znix.golfone.compiler.CompilationException;
import xyz.znix.golfone.compiler.G1Compiler;

/**
 * Created by znix on 3/13/17.
 */
public class BasicCompilationTests {
	@Test
	public void testSetVariable() throws CompilationException {
		/*
		  Set variable д to 1
		  Print variable д
		 */
		String program = "ρд~1ςπдς";
		System.out.println(program.length());

		G1Compiler compiler = new G1Compiler();

		compiler.compile(program);
	}
}
